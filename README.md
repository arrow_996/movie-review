# movie-review

This app allows the user to add the reviews to the movies and also mark the movies as favourite.

# bugs 
1. Make the Landing page image responsive

# features
1. Search functionality to search for the movies
2. Mark as favorite button
3. Review Form
4. Movie Profile

# Stack 
Angular 1 , Jquery and Bootstrap
