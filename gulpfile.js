var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


gulp.task('server', function() {
    browserSync.init({
    	server:'./'
    });
});

gulp.task('default',['server'], function() {
    // content

    gulp.watch('/*.html').on('change',reload);
    gulp.watch('/templates/*.html').on('change',reload);
    gulp.watch('app/css/*.css').on('change',reload);
    
});