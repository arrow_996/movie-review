/**
*  Module
*
* Description
*/
var myapp =angular.module('myapp', ['ui.router'])

myapp.config(function($stateProvider,$urlRouterProvider) {

           $urlRouterProvider.otherwise('/home');
           $stateProvider.state('home',{
              url:'/home',
              templateUrl:'templates/home.html',
             })
            
            .state('movies',{
           	 url:'/movies',
           	 views:
           	 {
           	 	'' :{
           	 		   templateUrl:'templates/about.html'
           	 		 },
           	     'movieList@movies':
           	         {
           	     	    templateUrl:'templates/movie-list.html',
           	     	    controller:'moviecontroller'
           	         },

           	 }
           	 
           	 
             })

            .state('TV-shows',{
            	url : '/TV-shows',
            	views:
           	 {
           	 	'' :{
           	 		   templateUrl:'templates/about.html'
           	 		 },
           	     'tvList@TV-shows':
           	         {
           	     	    templateUrl:'templates/tv-show.html',
           	     	    controller:'tvcontroller'
           	         },

           	 }
            });
        });


myapp.controller('moviecontroller',['$scope','$http', function($scope,$http){

	$http.get('apifiles/movie-intro.json')

	.then(function(data) {
		
		$scope.arr = [];
		$scope.data = data.data.movies;
		
       //convertong json object to array
		$scope.arr = Object.values($scope.data);
		//console.log($scope.arr)
		
});

}]);

myapp.controller('tvcontroller', ['$scope','$http', function($scope,$http){
                
	$http.get('apifiles/tv-show.json')

	.then(function(data) {
		
     $scope.arr = [];
     $scope.data =data.data.tvshow;
     $scope.arr = Object.values($scope.data)
     //console.log($scope.arr)
		
});


}]);




















